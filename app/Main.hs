{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE RecordWildCards #-}
module Main where

import           System.Environment             ( getArgs
                                                , getProgName
                                                )
import           Templating
import           System.Console.CmdArgs.Implicit
import           Data.Maybe                     ( fromMaybe )

data HifiArgs = HifiArgs { filename :: Maybe String
                         , interface :: Maybe Interface
                         , ssid :: Maybe SSID
                         , passphrase :: Maybe Passphrase
                         } deriving (Show, Data, Typeable)

hifiArgs =
  HifiArgs
      { filename   = def
        &= help "What the script and data files should be called."
        &= typ "FILE"
      , interface  = def
        &= help "Which interface should be used in the connect script."
        &= typ "STRING"
      , ssid       = def
        &= help "Which SSID the network you want to connect to has."
        &= typ "STRING"
      , passphrase = def
        &= help "What the passphrase for the network you want to connect to is."
        &= typ "STRING"
      }
    &= summary "Hifi"

main :: IO ()
main = do
  HifiArgs {..} <- cmdArgs hifiArgs
  case sequence [filename, interface, ssid, passphrase] of
    Just [filename, interface, ssid, passphrase] -> do
      (scriptFile, dataFile) <- createFiles filename interface ssid passphrase
      putStrLn $ "Created " ++ scriptFile ++ " and " ++ dataFile

    Nothing -> print
      "pass the '--help' flag to hifi to see which arguments are required"
