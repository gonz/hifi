-- | Handles directory management for both the template data and the
-- data/scripts that the user generates when using 'hifi'. Internally it uses
-- '$XDG_DATA_HOME' to locate the directory where scripts and data files should
-- end up.
--
-- This usually defaults to '$HOME/.local' in most Linux distributions,
-- but can also be set manually if one chooses to do so.
module Config
  ( templateDirectory
  , dataDirectory
  , scriptDirectory
  )
where

import           Paths_hifi
import           System.Directory

createIfMissingPath :: FilePath -> IO FilePath
createIfMissingPath path = createDirectoryIfMissing True path >> pure path

-- | 'xdgDirectory' finds the user's '$XDG_DATA_HOME' directory and appends a given
-- path to it. '$XDG_DATA_HOME' defaults to '$HOME/.local' in many Linux
-- distributions, but can also be set manually.
xdgDirectory :: FilePath -> IO FilePath
xdgDirectory = getXdgDirectory XdgData

-- | 'templateDirectory' will point to the directory where the supplied data files (templates)
-- end up. The data dir will, when using `stack` end up in the `.stack-work`
-- folder. This directory is then used in order to read the templates at runtime
-- so that they can be used when writing new script/data files.
templateDirectory :: IO FilePath
templateDirectory = getDataFileName "templates"

-- | 'dataDirectory' points to a folder in the XDG data directory where it will store
-- the data files used to connect to networks. The subdirectory that will be used
-- is '$XDG_DATA_HOME/hifi/data'. If this directory does not already exist when
-- 'dataDirectory' is evaluated, it will be created.
dataDirectory :: IO FilePath
dataDirectory = xdgDirectory "hifi/data" >>= createIfMissingPath

-- | 'scriptDirectory' points to a folder in the XDG data directory where it will store
-- the scripts it generates to connect to networks. The subdirectory that will be
-- used is '$XDG_DATA_HOME/hifi/scripts'. If this directory does not already exist when
-- 'scriptDirectory' is evaluated, it will be created.
scriptDirectory :: IO FilePath
scriptDirectory = xdgDirectory "hifi/scripts" >>= createIfMissingPath
